package alarmList;

import alarm.EnteredPinEvent;

public class SoundAlert implements AlarmListener {
	
	public void alarmTurnedOn(EnteredPinEvent event) {
		System.out.println("IJO IJO: " + event.getDate().toLocaleString());
	}
	
	public void alarmTurnedOff(EnteredPinEvent event) {
		System.out.println("Silence");
	}
}
