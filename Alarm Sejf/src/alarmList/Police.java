package alarmList;

import alarm.EnteredPinEvent;

public class Police implements AlarmListener {

	public void alarmTurnedOn(EnteredPinEvent event) {
		System.out.println("Police on the way: " + event.getDate().toLocaleString());
	}
	
	public void alarmTurnedOff(EnteredPinEvent event) {
		System.out.println("Police back to the police station");
	}
}
