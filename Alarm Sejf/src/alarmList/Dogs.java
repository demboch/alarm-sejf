package alarmList;

import alarm.EnteredPinEvent;

public class Dogs implements AlarmListener {

	public void alarmTurnedOn(EnteredPinEvent event) {
		System.out.println("Barking dogs: " + event.getDate().toLocaleString());
	}
	
	public void alarmTurnedOff(EnteredPinEvent event) {
		System.out.println("Good Dogs!!!");
	}
}
