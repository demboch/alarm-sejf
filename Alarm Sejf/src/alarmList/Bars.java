package alarmList;

import alarm.EnteredPinEvent;

public class Bars implements AlarmListener {

	public void alarmTurnedOn(EnteredPinEvent event) {
		System.out.println("Break bars: " + event.getDate().toLocaleString());
	}
	
	public void alarmTurnedOff(EnteredPinEvent event) {
		System.out.println("The bars not destroyed");
	}
}
