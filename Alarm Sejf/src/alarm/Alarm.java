package alarm;

import java.util.ArrayList;
import java.util.List;
import alarmList.AlarmListener;

public class Alarm {

	private List<AlarmListener> listeners = new ArrayList<AlarmListener>();
	private final String pin;
	
	public Alarm() {
		this("pin"); //this.pin = "pin";
	}
	
	public Alarm(String pin) {
		this.pin = pin;
	}
	
	public void addListener(AlarmListener listener) {
		this.listeners.add(listener);
	}
	
	public void removeListener(AlarmListener listener) {
		this.listeners.remove(listener);
	}
	
	public void enterPin(String pin) {
		System.out.println("\n Enter pin: " + pin);
		
		if(this.pin.equals(pin)) {
			this.correctPin();
		}
		
		else {
			this.wrongPin();
		}
	}
	
	private void correctPin()
	{
		for(AlarmListener alarm : listeners)
	           alarm.alarmTurnedOff(new EnteredPinEvent(this));
		System.out.println(" Correct pin! ");
	}
	
	private void wrongPin()
	{
		for(AlarmListener alarm : listeners)
	           alarm.alarmTurnedOn(new EnteredPinEvent(this));
		System.out.println(" Incorrect pin! ");
	}

	public String getPin() {
		return pin;
	}
}
