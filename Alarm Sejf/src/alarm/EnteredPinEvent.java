package alarm;

import java.util.Date;

public class EnteredPinEvent {

	private Alarm alarm;
	private Date date;
	
	public EnteredPinEvent(Alarm alarm) {
		this.alarm = alarm;
		this.date = new Date();
	}

	public Alarm getAlarm() {
		return alarm;
	}

	public Date getDate() {
		return date;
	}	
}
