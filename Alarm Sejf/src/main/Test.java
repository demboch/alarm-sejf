package main;

import alarm.Alarm;
import alarmList.Bars;
import alarmList.Dogs;
import alarmList.Police;
import alarmList.SoundAlert;

public class Test {

	public static void main(String[] args) {
		
		Alarm alarm = new Alarm();	
		
		Dogs dogs = new Dogs();
		alarm.addListener(dogs);
		alarm.enterPin("pin");
		
		SoundAlert alert = new SoundAlert();	
		alarm.addListener(alert);
		alarm.enterPin("pin");
		
		Police police = new Police();
		alarm.addListener(police);
		alarm.enterPin("zlypin");
		
		Bars bars = new Bars();
		alarm.addListener(bars);
		alarm.enterPin("pin");
		alarm.removeListener(bars);
		alarm.enterPin("zlypin");
		alarm.enterPin("pin");
	}
}
